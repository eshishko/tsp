<?php

namespace Application\Migrations;

use AppBundle\Entity\CitiesDistance;
use AppBundle\Entity\City;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171105182936 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var EntityRepository
     */
    private $citiesRepository;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema)
    {
        $this->em = $this->container->get('doctrine')->getManager();
        $citiesDistancerepo = $this->em->getRepository(CitiesDistance::class);
        $this->citiesRepository = $this->em->getRepository(City::class);

        $cities = [
            'Riga' => [
                'Daugavpils' => 223,
                'Liepaja' => 215,
                'Jelgava' => 44,
                'Jurmala' => 25,
                'Ventspils' => 188,
                'Rezekne' => 240,
                'Jekabpils' => 139,
                'Valmiera' => 127,
                'Ogre' => 37,
                'Tukums' => 67,
                'Cesis' => 95,
                'Salaspils' => 21,
            ],
            'Daugavpils' => [
                'Liepaja' => 427,
                'Jelgava' => 246,
                'Jurmala' => 245,
                'Ventspils' => 407,
                'Rezekne' => 89,
                'Jekabpils' => 91,
                'Valmiera' => 249,
                'Ogre' => 194,
                'Tukums' => 286,
                'Cesis' => 223,
                'Salaspils' => 213,
            ],
            'Liepaja' => [
                'Jelgava' => 180,
                'Jurmala' => 200,
                'Ventspils' => 117,
                'Rezekne' => 449,
                'Jekabpils' => 345,
                'Valmiera' => 322,
                'Ogre' => 253,
                'Tukums' => 169,
                'Cesis' => 309,
                'Salaspils' => 236,
            ],
            'Jelgava' => [
                'Jurmala' => 56,
                'Ventspils' => 175,
                'Rezekne' => 279,
                'Jekabpils' => 163,
                'Valmiera' => 154,
                'Ogre' => 71,
                'Tukums' => 54,
                'Cesis' => 136,
                'Salaspils' => 55,
            ],
            'Jurmala' => [
                'Ventspils' => 266,
                'Rezekne' => 266,
                'Jekabpils' => 161,
                'Valmiera' => 152,
                'Ogre' => 71,
                'Tukums' => 43,
                'Cesis' => 118,
                'Salaspils' => 54,
            ],
            'Ventspils' => [
                'Rezekne' => 430,
                'Jekabpils' => 325,
                'Valmiera' => 294,
                'Ogre' => 234,
                'Tukums' => 123,
                'Cesis' => 282,
                'Salaspils' => 217,
            ],
            'Rezekne' => [
                'Jekabpils' => 107,
                'Valmiera' => 193,
                'Ogre' => 208,
                'Tukums' => 315,
                'Cesis' => 196,
                'Salaspils' => 225,
            ],
            'Jekabpils' => [
                'Valmiera' => 154,
                'Ogre' => 108,
                'Tukums' => 203,
                'Cesis' => 137,
                'Salaspils' => 127,
            ],
            'Valmiera' => [
                'Ogre' => 112,
                'Tukums' => 173,
                'Cesis' => 32,
                'Salaspils' => 114,
            ],
            'Ogre' => [
                'Tukums' => 112,
                'Cesis' => 91,
                'Salaspils' => 20,
            ],
            'Tukums' => [
                'Cesis' => 161,
                'Salaspils' => 102,
            ],
            'Cesis' => [
                'Salaspils' => 96,
            ],
        ];

        $count = 0;
        foreach ($cities as $fromCityName => $toCities) {
            $fromCity = $this->getCity($fromCityName);
            foreach ($toCities as $toCityName => $distance) {
                $toCity = $this->getCity($toCityName);
                $citiesDistance = (new CitiesDistance())
                    ->setFromCity($fromCity)
                    ->setToCity($toCity)
                    ->setDistance($distance);
                $this->em->persist($citiesDistance);

                if (++$count === 50) {
                    $count = 0;
                    $this->em->flush();
                }
            }
        }

        $this->em->flush();
    }

    private function getCity(string $name)
    {
        $city = $this->citiesRepository->findOneBy(['name' => $name]);
        if (!$city) {
            $city = (new City())->setName($name);
            $this->em->persist($city);
            $this->em->flush();
        }

        return $city;
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
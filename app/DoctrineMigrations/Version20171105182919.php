<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171105182919 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tour (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, ip_address VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tour_city (tour_id INT NOT NULL, city_id INT NOT NULL, INDEX IDX_C6BE21415ED8D43 (tour_id), INDEX IDX_C6BE2148BAC62AF (city_id), PRIMARY KEY(tour_id, city_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cities_distance (id INT AUTO_INCREMENT NOT NULL, from_city_id INT DEFAULT NULL, to_city_id INT DEFAULT NULL, distance INT NOT NULL, INDEX IDX_810BD331DF28100 (from_city_id), INDEX IDX_810BD3315345F5A (to_city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE city (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE city_tour (city_id INT NOT NULL, tour_id INT NOT NULL, INDEX IDX_899FC4018BAC62AF (city_id), INDEX IDX_899FC40115ED8D43 (tour_id), PRIMARY KEY(city_id, tour_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tour_city ADD CONSTRAINT FK_C6BE21415ED8D43 FOREIGN KEY (tour_id) REFERENCES tour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tour_city ADD CONSTRAINT FK_C6BE2148BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE cities_distance ADD CONSTRAINT FK_810BD331DF28100 FOREIGN KEY (from_city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE cities_distance ADD CONSTRAINT FK_810BD3315345F5A FOREIGN KEY (to_city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE city_tour ADD CONSTRAINT FK_899FC4018BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE city_tour ADD CONSTRAINT FK_899FC40115ED8D43 FOREIGN KEY (tour_id) REFERENCES tour (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tour_city DROP FOREIGN KEY FK_C6BE21415ED8D43');
        $this->addSql('ALTER TABLE city_tour DROP FOREIGN KEY FK_899FC40115ED8D43');
        $this->addSql('ALTER TABLE tour_city DROP FOREIGN KEY FK_C6BE2148BAC62AF');
        $this->addSql('ALTER TABLE cities_distance DROP FOREIGN KEY FK_810BD331DF28100');
        $this->addSql('ALTER TABLE cities_distance DROP FOREIGN KEY FK_810BD3315345F5A');
        $this->addSql('ALTER TABLE city_tour DROP FOREIGN KEY FK_899FC4018BAC62AF');
        $this->addSql('DROP TABLE tour');
        $this->addSql('DROP TABLE tour_city');
        $this->addSql('DROP TABLE cities_distance');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE city_tour');
    }
}

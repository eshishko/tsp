<?php

namespace AppBundle\Repository;

use AppBundle\Entity\City;
use Doctrine\ORM\EntityRepository;

final class CitiesDistanceRepository extends EntityRepository
{
    /**
     * @param City $fromCity
     * @param City $toCity
     *
     * @return mixed
     */
    public function getDistance(City $fromCity, City $toCity)
    {
        return $this->createQueryBuilder('dc')
            ->where('dc.fromCity = :fromCity AND dc.toCity = :toCity')
            ->orWhere('dc.toCity = :fromCity AND dc.fromCity = :toCity')
            ->setParameter('toCity', $toCity)
            ->setParameter('fromCity', $fromCity)
            ->getQuery()
            ->getSingleResult();
    }
}
<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CitiesDistance;
use AppBundle\Entity\City;
use AppBundle\Entity\Tour;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class WebsiteController extends BaseController
{
    /**
     * @Route("/", name="homepage")
     *
     * @Method({"GET"})
     */
    public function indexAction(Request $request)
    {
        $exampleTour = $this->getTourRepository()
            ->findOneBy(['name' => 'Example Tour']);

        return $this->render('@App/default/default.html.twig', [
            'tours' => $this->getAllTours($request->getClientIp()),
            'example_tour' => $exampleTour
        ]);
    }

    /**
     * @Route("/tours", name="tours")
     *
     * @Method({"GET"})
     */
    public function toursAction(Request $request)
    {
        return $this->render('@App/tours/tours.html.twig', [
            'tours' => $this->getAllTours($request->getClientIp()),
        ]);
    }

    /**
     * @Route("/tour/{tour}", name="tour.list")
     *
     * @Method({"GET"})
     */
    public function tourListAction(Request $request, Tour $tour)
    {
        if ($tour->getIpAddress() !== $request->getClientIp()) {
            throw new NotFoundHttpException('Tour was not found.');
        }

        return $this->render('@App/tour/tour.html.twig', [
            'tour' => $tour,
        ]);
    }

    /**
     * @Route("/tour/{tour}", name="tour.delete")
     *
     * @Method({"DELETE"})
     */
    public function tourDeleteAction(Request $request, Tour $tour)
    {
        if ($tour->getIpAddress() !== $request->getClientIp()) {
            throw new NotFoundHttpException('Tour was not found.');
        }
        $em = $this->getEntityManager();
        $em->remove($tour);
        $em->flush();

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/tour", name="tour")
     *
     * @Method({"GET", "POST"})
     */
    public function tourAction(Request $request)
    {
        $tour = new Tour();
        $form = $this->createFormBuilder($tour)
            ->add('name', TextType::class, ['label' => 'Tour Name (label)'])
            ->add('cities', EntityType::class, [
                'class' => City::class,
                'choice_label' => 'name',
                'multiple' => true,
                'attr' => ['data-select' => 'true']
            ])
            ->getForm()
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Tour $tour */
            $tour = $form->getData();
            $tour->setIpAddress($request->getClientIp());
            $em = $this->getEntityManager();
            $em->persist($tour);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('@App/tour/tour_edit.html.twig', [
            'citiesDistances' => $this->getAllCitiesDistances(),
            'form' => $form->createView()
        ]);
    }

    /**
     * @param string $ipAddress
     *
     * @return Tour[]|array
     */
    private function getAllTours(string $ipAddress)
    {
        return $this->getTourRepository()
            ->findBy(['ipAddress' => $ipAddress]);
    }

    /**
     * @return array
     */
    private function getAllCitiesDistances()
    {
        return $this->getCitiesDistanceRepository()
            ->findAll();
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function getCitiesDistanceRepository()
    {
        return $this->getEntityManager()
            ->getRepository(CitiesDistance::class);
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function getTourRepository()
    {
        return $this->getEntityManager()
            ->getRepository(Tour::class);
    }
}

<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\City;

abstract class BaseController extends Controller
{
    /**
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @return City[]|array
     */
    protected function getAllCities()
    {
        return $this->getCityRepository()
            ->findAll();
    }

    /**
     * @param string $cityName
     *
     * @return null|object
     */
    protected function findCity(string $cityName)
    {
        return $this->getCityRepository()
            ->findOneBy(['name' => $cityName]);
    }

    /**
     * @return \AppBundle\Repository\CityRepository
     */
    protected function getCityRepository()
    {
        return $this->getEntityManager()
            ->getRepository(City::class);
    }
}
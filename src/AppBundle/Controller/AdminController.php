<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CitiesDistance;
use AppBundle\Entity\City;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Support\StringSupport;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

final class AdminController extends BaseController
{
    /**
     * @Route("/admin/city", name="city")
     * @Method({"GET"})
     */
    public function cityAction(Request $request)
    {
        return $this->render('@App/city/city.html.twig', [
            'cities' => $this->getAllCities()
        ]);
    }

    /**
     * @Route("/admin/city", name="new_city")
     * @Method({"POST"})
     */
    public function newCityAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $allData = $request->request->all();
        $fromCity = (new City())->setName($allData['city']);
        $em->persist($fromCity);

        foreach ($allData as $cityKey => $distance) {
            if (StringSupport::endsWith($cityKey, '_distance')) {
                $toCityName = substr($cityKey, 0, -(strlen('_distance')));;
                $toCity = $this->findCity($toCityName);
                $cityDistance = (new CitiesDistance())
                    ->setFromCity($fromCity)
                    ->setDistance($distance)
                    ->setToCity($toCity);
                $em->persist($cityDistance);
            }
        }

        $em->flush();

        return $this->redirectToRoute('city');
    }
}
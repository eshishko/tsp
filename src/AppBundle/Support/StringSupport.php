<?php

namespace AppBundle\Support;

final class StringSupport
{
    /**
     * @param string $string
     * @param string $start
     *
     * @return bool
     */
    public static function startsWith(string $string, string $start)
    {
        return substr($string, 0, strlen($start)) === $start;
    }

    /**
     * @param string $string
     * @param string $end
     *
     * @return bool
     */
    public static function endsWith(string $string, string $end)
    {
        return substr($string, -strlen($end)) == $end;
    }

    /**
     * @param string $string
     * @param string $substring
     *
     * @return bool
     */
    public static function contains(string $string, string $substring)
    {
        return strpos($string, $substring) !== false;
    }
}
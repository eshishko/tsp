<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="tour")
 */
class Tour
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var City[]
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\City")
     * @Assert\Count(min = 3)
     */
    private $cities;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_address", type="string")
     */
    private $ipAddress;

    public function __construct()
    {
        $this->cities = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): Tour
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return City[]|null
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param $cities
     *
     * @return $this
     */
    public function setCities($cities): Tour
    {
        if (count($cities) > 0) {
            foreach ($cities as $city) {
                $this->addCity($city);
            }
        }

        return $this;
    }

    /**
     * @param City $city
     */
    public function addCity(City $city)
    {
        if ($this->cities->contains($city)) {
            return;
        }

        $this->cities->add($city);
        $city->addTour($this);
    }

    /**
     * @param City $city
     */
    public function removeCity(City $city)
    {
        if (!$this->cities->contains($city)) {
            return;
        }

        $this->cities->removeElement($city);
        $city->removeTour($this);
    }

    /**
     * @return string
     */
    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     *
     * @return $this
     */
    public function setIpAddress(string $ipAddress): Tour
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }
}
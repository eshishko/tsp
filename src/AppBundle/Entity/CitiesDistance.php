<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CitiesDistanceRepository")
 *
 * @ORM\Table(name="cities_distance")
 */
class CitiesDistance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\City
     *
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\City")
     */
    private $fromCity;

    /**
     * @var \AppBundle\Entity\City
     *
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\City")
     */
    private $toCity;

    /**
     * @var int
     *
     * @ORM\Column(name="distance", type="integer")
     */
    private $distance;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return City
     */
    public function getFromCity(): City
    {
        return $this->fromCity;
    }

    /**
     * @param City $fromCity
     *
     * @return CitiesDistance
     */
    public function setFromCity(City $fromCity): CitiesDistance
    {
        $this->fromCity = $fromCity;

        return $this;
    }

    /**
     * @return City
     */
    public function getToCity(): City
    {
        return $this->toCity;
    }

    /**
     * @param City $toCity
     *
     * @return CitiesDistance
     */
    public function setToCity(City $toCity): CitiesDistance
    {
        $this->toCity = $toCity;

        return $this;
    }

    /**
     * @return int
     */
    public function getDistance(): int
    {
        return $this->distance;
    }

    /**
     * @param int $distance
     *
     * @return $this
     */
    public function setDistance(int $distance): CitiesDistance
    {
        $this->distance = $distance;

        return $this;
    }
}
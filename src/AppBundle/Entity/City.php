<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="city")
 * @UniqueEntity(fields={"name"})
 */
class City
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var Tour[]
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Tour")
     */
    private $tours;

    public function __construct()
    {
        $this->tours = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): City
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Tour[]
     */
    public function getTours(): array
    {
        return $this->tours;
    }

    /**
     * @param Tour $tour
     */
    public function addTour(Tour $tour)
    {
        if ($this->tours->contains($tour)) {
            return;
        }

        $this->tours->add($tour);
        $tour->addCity($this);
    }

    /**
     * @param Tour $tour
     */
    public function removeTour(Tour $tour)
    {
        if (!$this->tours->contains($tour)) {
            return;
        }

        $this->tours->removeElement($tour);
        $tour->removeCity($this);
    }
}
<?php

namespace AppBundle\Twig;

use AppBundle\Entity\CitiesDistance;
use AppBundle\Entity\City;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Twig_SimpleFunction;

final class DistanceExtension extends \Twig_Extension
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $em;

    /**
     * @param RegistryInterface $doctrine
     */
    public function __construct(RegistryInterface $doctrine)
    {
        $this->em = $doctrine->getManager();
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('distance', [$this, 'getDistance']),
        ];
    }

    /**
     * @param City $fromCity
     * @param City $toCity
     *
     * @return mixed
     */
    public function getDistance(City $fromCity, City $toCity)
    {
        return $this->getCitiesDistanceRepository()
            ->getDistance($fromCity, $toCity)
            ->getDistance();
    }

    /**
     * @return \AppBundle\Repository\CitiesDistanceRepository
     */
    private function getCitiesDistanceRepository()
    {
        return $this->em
            ->getRepository(CitiesDistance::class);
    }
}
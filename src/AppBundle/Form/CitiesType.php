<?php

namespace AppBundle\Form;

use AppBundle\Entity\City;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

final class CitiesType extends AbstractType
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $em;

    /**
     * @param RegistryInterface $doctrine
     */
    public function __construct(RegistryInterface $doctrine)
    {
        $this->em = $doctrine->getManager();
    }

//    public function buildForm(FormBuilderInterface $builder, array $options)
//    {
//        foreach ($this->getCities() as $city) {
//            $builder->add($city->getName());
//        }
////
////        $builder->add('name');
////        $builder->add('price');
//    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $choices = [];
        foreach ($this->getCities() as $city) {
            $choices[$city->getName()] = $city->getId();
        }
        ksort($choices);
        // SF 2.7+ BC
//        if (method_exists('Symfony\Component\Form\AbstractType', 'configureOptions')) {
//            $choices = array_flip($choices);
//            ksort($choices);
//        }
        $resolver->setDefaults([
            'choices' => array_flip($choices),
            'choices_as_values' => true,
        ]);
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return ChoiceType::class;
    }

    /**
     * @return string
     */
//    public function getBlockPrefix()
//    {
//        return 'cities_type';
//    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @return City[]|array
     */
    private function getCities()
    {
        return $this->em
            ->getRepository(City::class)
            ->findAll();
    }
}
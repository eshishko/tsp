<?php

namespace ApiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final class ApiController extends Controller
{
    /**
     * @Route("/", name="ppp")
     */
    public function indexAction(Request $request)
    {
        $data = ['tets'];

        return new JsonResponse($data);
    }
}
